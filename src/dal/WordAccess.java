package dal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The WordAccess class manages the way word-lists are loaded into the application.
 * 
 * @author KHR
 */
public class WordAccess{

    /* Instance variables */
    private static WordAccess instance = null;
    private List<String> items = null;
    private File file;

    /**
     * A 'blank' constructor. This is private because it utilizes a Singleton Pattern.
     */
    private WordAccess() {
        // Blank
    }

    /**
     * @return an instance of the WordAccess class.
     */
    public static WordAccess getInstance(){
        if( instance == null ) instance = new WordAccess();
        return instance;
    }

    /**
     * Loads a file into the WordAccess class.
     * 
     * @param f file to load.
     * @throws FileNotFoundException if the file is not found.
     */
    public void loadFile( File f ) throws FileNotFoundException{
        this.file = f;
        FileReader fr = new FileReader( file );
        BufferedReader br = new BufferedReader( fr );
        Scanner sc = new Scanner( br );
        items = new ArrayList<>();
        while( sc.hasNextLine() ){
            items.add( sc.nextLine() );
        }
        sc.close();
    }

    /**
     * Loads a file into the WordAccess class.
     * 
     * @param path path to the file to load.
     * @throws FileNotFoundException if the file is not found.
     */
    public void loadFile( String path ) throws FileNotFoundException{
        File f = new File( path );
        loadFile( f );
    }

    /**
     * @return a list of items retrieved from the last loaded file.
     * @throws NullPointerException if no file has been loaded.
     */
    public List<String> getItems() throws NullPointerException{
        if( items == null ) throw new NullPointerException( "No file loaded yet." );
        return items;
    }

    /**
     * @return the last loaded file.
     */
    public File getFile(){
        return file;
    }
}
