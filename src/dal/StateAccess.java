package dal;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import be.WindowState;

/**
 * This class saves and loads the WindowState to and from a file.
 * @author KHR
 *
 */
public class StateAccess{

    /* Instance fields */
    // State file parameters
    private static final String STATE_FILE_PATH = "system.sys";
    private static final File stateFile = new File( STATE_FILE_PATH );

    // Default values
    private static final Rectangle bounds = null;
    private static final boolean showStartScreen = true;
    private static final File file = null;

    /**
     * Saves a given WindowState to a file.
     * 
     * @param state the WindowState to save.
     * @throws FileNotFoundException if the file does not exist. (It will though, is it is created if it
     *             doesn't exist.)
     * @throws IOException if the file is a directory, or read/write permission is denied.
     */
    public static void saveState( WindowState state ) throws FileNotFoundException, IOException{
        ObjectOutputStream oos = null;
        FileOutputStream fos = null;
        try{
            if( !stateFile.exists() ) stateFile.createNewFile();
            fos = new FileOutputStream( stateFile );
            oos = new ObjectOutputStream( fos );
            oos.writeObject( state );
        }
        finally{
            oos.close();
            fos.close();
        }
    }

    /**
     * Attempts to load a WindowState from a file.
     * 
     * @return the loaded WindowState, or a default WindowState, if the file does not exist.
     * @throws IOException if the file is a directory, or read/write permission is denied.
     * @throws ClassNotFoundException if the file does not contain an object of the WindowState class.
     */
    public static WindowState loadState() throws IOException, ClassNotFoundException{
        WindowState state = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try{
            if( stateFile.exists() ){
                fis = new FileInputStream( stateFile );
                ois = new ObjectInputStream( fis );
                state = (WindowState) ois.readObject();
            }else if( !stateFile.exists() || state == null ) state = createDefaultState();
        }
        finally{
            if( ois != null ) ois.close();
            if( fis != null ) fis.close();
        }
        return state;
    }

    /**
     * @return a default WindowState.
     */
    private static WindowState createDefaultState(){
        return new WindowState( bounds, file, showStartScreen );
    }
}
