package application;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import ui.ExceptionHandler;
import ui.StartupWindow;

/**
 * The main class of the application
 * @author KHR
 *
 */
public class MainClass{

    /**
     * Launches the application.
     * @param args application arguments. (unused)
     */
    public static void main( String[] args ){
        SwingUtilities.invokeLater( new Runnable(){
            @Override
            public void run(){
                try{
                    /* Set the native look and feel */
                    UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
                    /* Start the program */
                    new StartupWindow();
                }
                catch( Exception e ){
                    ExceptionHandler.handle( e );
                }
            }
        } );
    }
}
