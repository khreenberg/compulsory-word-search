package ui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ui.models.CollectionListModel;
import be.SearchObject;
import be.WindowState;
import bll.ISearchStyle;
import bll.StateManager;
import bll.WordManager;

/**
 * The main window for the application.
 * 
 * @author KHR
 */
@SuppressWarnings( { "rawtypes", "unchecked" } )
public class WordWindow implements ISearchStyle, PropertyChangeListener{

    /* Instance fields */
    // These are used to do a switch/case on the actionCommand-String tied to the radiobuttons
    private final String CONTAINS = "contains";
    private final String BEGINS = "begin";
    private final String ENDS = "end";
    private final String EXACT = "exact";

    /* Instance variables */
    private HashMap<KeyStroke, Action> hotkeys = new HashMap<KeyStroke, Action>();    // The HashMap containing our hotkeys
    private StyleListener styleListener = new StyleListener();  // This used to be the actionListener for the radio buttons
    @SuppressWarnings( "unused" )                               // Supress warnings after converting to getSearchStyle()
    private Style searchStyle = Style.CONTAIN;                  // Deprecated - See the method 'getSearchStyle()' instead
    
    private WindowState state;                 // The WindowState
    private WordManager wm;                    // WordManager variable
    private int resultAmount = -1;             // The initial amount of search results to show. Negative values means "show all"
    private boolean searching = false;         // Alternatively, one could use setActionCommand on the search button.

    private List<String> completeResult = new ArrayList<>();    // This list holds the complete result
    private List<String> maskedResult;                          // This list holds the first n-results based on the limitation combobox

    private SearchTask search;  // This holds the SearchTask, so that we may read from and interrupt it anywhere in this class.

    private CollectionListModel lstModel = new CollectionListModel<String>( new ArrayList<String>() );  // Starts the result view with an empty list 

    /* Components */
    private JFrame frmWordSearch;   // -------------------------- The main frame of the window
    private JLabel lblQuery;        // -------------------------- The label that says 'Query:' in front of the search field
    private JTextField textQuery;   // -------------------------- The search field
    private JButton btnSearch;      // -------------------------- The search button
    private JButton btnClear;       // -------------------------- The clear button
    private JLabel lblResult;       // -------------------------- The label that says 'Result:' in front of the result list
    private JScrollPane scrollPane; // -------------------------- The scroll pane that holds the result list
    private JList<?> lstResult;     // -------------------------- The result list
    private JLabel lblMatches;      // -------------------------- The label that says 'Matches:' before the number of search results
    private JLabel lblMatchcount;   // -------------------------- The label that displays the number of search results
    private JPanel pnlSearchType;   // -------------------------- The panel that holds the radio buttons
    private ButtonGroup buttonGroup = new ButtonGroup();    // -- The buttonGroup that groups the radio buttons
    private JRadioButton rdbtnContains; // ---------------------- The radio button that represents the search containing the search string
    private JRadioButton rdbtnBeginsWith;   // ------------------ The radio button that represents the search beginning with the search string
    private JRadioButton rdbtnEndsWith; // ---------------------- The radio button that represents the search ending with the search string
    private JRadioButton rdbtnExact;    // ---------------------- The radio button that represents the search for words matching the search string
    private JPanel pnlStyle;    // ------------------------------ The panel that holds the 'Case Sensitive'-checkbox
    private JCheckBox chckbxCaseSensitive;  // ------------------ The 'Case Sensitive'-checkbox
    private JPanel pnlLimitation;   // -------------------------- The panel that holds the 'Limitations'-checkbox
    private JComboBox<?> comboBox;  // -------------------------- The 'Limitations'-checkbox
    private JMenuBar menuBar;   // ------------------------------ The MenuBar
    private JMenu mnFile;       // ------------------------------ The File menu
    private JMenuItem mntmOpenFile; // -------------------------- The 'Open File' option in the file menu
    private JSeparator separator_1; // -------------------------- The seperator between 'Open File' & 'Exit'
    private JMenuItem mntmExit; // ------------------------------ The Exit option in the file menu
    private JMenu mnHelp;       // ------------------------------ The help menu
    private JCheckBoxMenuItem chckbxmntmShowFilePrompt; // ------ The checkbox controlling whether or not to show the startup window in the help menu
    private JSeparator separator;   // -------------------------- The seperator in the help menu
    private JMenuItem mntmAbout;    // -------------------------- The 'About' option in the help menu
    private JProgressBar progressBar;   // ---------------------- The progressbar

    /**
     * Create and show the window
     * @param state the state to base the window on
     */
    public WordWindow( WindowState state ) {
        wm = WordManager.getInstance();
        initialize();               // Create the components
        placeComponents();          // Place the components
        createHotkeys();            // Create our hotkeys and accelerators
        applyWindowState( state );  // Apply the WindowState
        /* Show the window */
        frmWordSearch.setVisible( true );   // Show the window
    }

    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい
    // # --------------------- LISTENERS ------------------------ #
    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい

    @SuppressWarnings( "serial" )
    private void createHotkeys(){
        /* Accelerators */
        mntmOpenFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        
        /* Mnemonics */
        btnSearch.setMnemonic( 's' );               // Alt+S to Search
        btnClear.setMnemonic( 'c' );                // Alt+C to Clear
        rdbtnContains.setMnemonic( 'o' );           // Alt+O to select "Contains"
        rdbtnBeginsWith.setMnemonic( 'b' );         // Alt+B to select "Begins With"
        rdbtnEndsWith.setMnemonic( 'e' );           // Alt+E to select "Ends With"
        rdbtnExact.setMnemonic( 'x' );              // Alt+X to select "Exact"
        chckbxCaseSensitive.setMnemonic( 'a' );     // Alt+A to toggle "Case Sensitive"
        mnFile.setMnemonic( 'f' );                  // Alt+F to open the File menu..
        mntmOpenFile.setMnemonic( 'o' );            // -> O to open a file
        mntmExit.setMnemonic( 'x' );                // -> X to exit the application
        mnHelp.setMnemonic( 'h' );                  // Alt+H to open the Help menu..
        chckbxmntmShowFilePrompt.setMnemonic('s');  // -> S to toggle whether or not to display the startup screen
        mntmAbout.setMnemonic( 'a' );               // -> A to display the About dialog
        
        /* Keystrokes */
        KeyStroke enter = KeyStroke.getKeyStroke( KeyEvent.VK_ENTER, 0 );                   // Enter
        KeyStroke escape = KeyStroke.getKeyStroke( KeyEvent.VK_ESCAPE, 0 );                 // Escape
        KeyStroke ctrlL = KeyStroke.getKeyStroke( KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK ); // Ctrl + L
        KeyStroke ctrlS = KeyStroke.getKeyStroke( KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK ); // Ctrl + S
        
        /* Actions */
        // Create them
        AbstractAction enterPressed = new AbstractAction("enter pressed"){
            @Override
            public void actionPerformed( ActionEvent ae ){
                btnSearch.doClick();
            }
        };
        AbstractAction excapePressed = new AbstractAction("escape pressed"){
            @Override
            public void actionPerformed( ActionEvent ae ){
                exitRoutine();
            }
        };
        AbstractAction ctrlLPressed = new AbstractAction(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                comboBox.requestFocus();
            }
        };
        AbstractAction ctrlSPressed = new AbstractAction(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                textQuery.requestFocusInWindow();
                textQuery.selectAll();
            }
        };
        // Map them
        hotkeys.put( enter, enterPressed );
        hotkeys.put( ctrlL, ctrlLPressed );
        hotkeys.put( ctrlS, ctrlSPressed );
        hotkeys.put( escape, excapePressed );
        
        /* Keyboard Focus Manager */
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();   // Save the current KeyboardFocusManager
        kfm.addKeyEventDispatcher( new KeyEventDispatcher(){                                // Add a key handler thingy
            @Override
            public boolean dispatchKeyEvent( KeyEvent e ){                              // When a KeyStroke is pressed..                          
                KeyStroke keyStroke = KeyStroke.getKeyStrokeForEvent( e );              // ..save it..
                if( hotkeys.containsKey( keyStroke ) ){                                 // ..if it's defined in our HashMap 'hotkeys'..
                    Action a = hotkeys.get( keyStroke );                                // ..get the corresponding action..
//                  ActionEvent ae = new ActionEvent( e.getSource(), e.getID(), null ); // ..(Not really used in this application)..
                    a.actionPerformed( null );                                          // ..and execute it!
                    return true;                                                        // We then return true, to tell other potential KeyEventDispatchers, that the keyStroke has been dealt with.
                }
                return false;   // If the keyStroke is not defined in our HashMap 'hotkeys', we return false, so that other potential KeyEventDispatchers may handle it (if neccessary).
            }
        } );
    }
    
    /**
     * Method to run when the search button is pressed
     */
    private void searchPressed(){
        if( !searching ){                               // If we're not searching already..
            uiBusy();                                   // ..tell the UI we're busy..
            search = new SearchTask();                  // ..create a new search task..
            search.addPropertyChangeListener( this );   // ..add our PropertyChangeListener (to listen for changes to the Task's progress)..
            search.execute();                           // ..and start our search task.
        }else{                                          // If we are already searching..
            search.cancel( true );                      // ..send a signal to cancel the search..
            uiReady();                                  // ..and tell the UI we're ready to search again.
        }
        textQuery.requestFocusInWindow();               // Then we give the search field the input focus.
    }

    /**
     * Method to run when a result is seected from the list
     */
    private void resultSelected(){
        textQuery.setText( lstResult.getSelectedValue().toString() );   // Put the text of the selected item into the search field.
    }

    /**
     * Method to update the search result
     */
    private void updateSearch(){
        lstModel.setCollection( maskedResult );     // Push our masked result into the list view..
        lblMatchcount.setText( String.format( "%s of %s", maskedResult.size(), completeResult.size() ) ); // ..and update the matchCount label.
    }

    /**
     * Method to run when the clear button is pressed
     */
    private void clearSearch(){
        textQuery.setText( "" );                        // Clear the text in the search field.
        lstModel.setCollection( new ArrayList<>() );    // Clear the result list.
        lblMatchcount.setText( "0" );                   // And set the search matches to 0.
    }

    /**
     * Method to run when the 'Open file..'-menu item is pressed
     */
    private void openFile(){
        try{
            JFileChooser jfc = new JFileChooser();          // Show a file chooser
            int option = jfc.showOpenDialog( null );        // Save whether the user presses 'OK' or 'Cancel'
            if( option == JFileChooser.APPROVE_OPTION ){    // If they press 'OK'..
                File f = jfc.getSelectedFile();             // ..save the chosen file..
                wm.loadFile( f );                           // ..attempt to load it..
                clearSearch();                              // ..and clear any old results.
            }
        }
        catch( Exception e ){
            ExceptionHandler.handle( e );
        }
    }

    /**
     * Method to run when a new limitation is selected
     */
    private void limitationBoxActionPerformed(){
        Object selected = comboBox.getSelectedItem();                           // Save the selected object..
        if( selected != null ){                                                 // ..if this object exists..
            try{                                                                // ..attempt to..
                String strSelect = (String) selected;                           // ..convert it to a String..
                if( strSelect.equalsIgnoreCase( ("None") ) ){                   // ..and if this string matches 'None'..
                    resultAmount = -1;                                          // ..show all results..
                }else{                                                          // ..if it doesn't match 'None'..
                    resultAmount = Integer.parseInt( strSelect );               // ..save the number picked.
                }
                if( 0 < resultAmount && resultAmount < completeResult.size() ){ // If there are more results than the user would like shown..
                    maskedResult = new ArrayList<String>();                     // ..create a new list..
                    for( int i = 0; i < resultAmount; ++i ){                    // ..and fill it with the first few results.
                        maskedResult.add( completeResult.get( i ) );
                    }
                }else maskedResult = completeResult;                            // If there are less results than what the user wants shown, just use them all.
                updateSearch();                                                 // Show the results.
            }
            catch( Exception e ){
                ExceptionHandler.handle( e );
            }
        }
    }

    /**
     * Method to run when the user tries to exit the application
     */
    private void exitRoutine(){
        try{
            state.setFile( wm.getFile() );
            state.setBounds( frmWordSearch.getBounds() );
            state.setShowStartScreen( chckbxmntmShowFilePrompt.isSelected() );
            StateManager.saveState( state );
            System.exit( 0 );
        }
        catch( Exception e ){
            ExceptionHandler.handle( e );
        }
    }

    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい
    // # ---------------------- UI STATES ----------------------- #
    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい

    /**
     * Sets the UI to the ready state
     */
    private void uiReady(){
        searching = false;
        btnSearch.setText( "Search" );
        frmWordSearch.setCursor( null );
        progressBar.setValue( 100 );
        progressBar.setString( "Ready" );
    }

    /**
     * Sets the UI to the busy state
     */
    private void uiBusy(){
        searching = true;
        btnSearch.setText( "Cancel" );
        frmWordSearch.setCursor( Cursor.getPredefinedCursor( Cursor.WAIT_CURSOR ) );
        progressBar.setValue( 0 );
    }

    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい
    // # -------------------- INNER CLASSES --------------------- #
    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい

    /**
     * This inner class extends SwingWorker, so that it may start a search in a background thread.
     * Alternatively, one could have just created an anonymous inner class in the searchPressed() method, but
     * this looks prettier.
     * 
     * @author KHR
     */
    private class SearchTask extends SwingWorker<Void, Void>{
        
        /**
         * This method should be used instead of the StyleListener inner class.
         * @return the selected search style.
         */
        private Style getSearchStyle(){
            Style style = Style.CONTAIN;
            switch( buttonGroup.getSelection().getActionCommand() ){
                case CONTAINS:
                    style = Style.CONTAIN;
                    break;
                case BEGINS:
                    style = Style.BEGIN;
                    break;
                case ENDS:
                    style = Style.END;
                    break;
                case EXACT:
                    style = Style.EXACT;
                    break;
            }
            return style;
        }
        
        @Override
        protected Void doInBackground() throws Exception{   // This method is automatically started in a background thread, when the SearchTask is excecuted.
            setProgress( 0 );       // Sets the task progress to 0
            int progress = 0;       // Save progress to a variable
            
            /* SearchObject parameters */
            String searchString = textQuery.getText();  // Save the search string (This is just for readability)
            Style style = getSearchStyle();
            boolean caseSensitive = chckbxCaseSensitive.isSelected();   // Save whether the search should be case sensitive (Also just for readability)
            SearchObject so = new SearchObject( searchString, style, caseSensitive ); // Create a new SearchObject with the given information. 
            
            /* Search */
            wm.doSearch( so );  // Start the search
            while( progress < 100 && !isCancelled() ){ // While the search is not done or cancelled
                progress = wm.getProgress();                // Update the progress variable..
                setProgress( Math.min( progress, 100 ) );   // ..and the progress of the task.
            }
            if( isCancelled() ) wm.killSearch();    // If the task got cancelled (by pressing the cancel button), send a signal to stop the search in the Logic layer.
            completeResult = wm.getResult();        // Get the result from the logic layer
            limitationBoxActionPerformed();         // Pretend the user clicked the limitation combobox (This will update the List 'maskedResult'
            updateSearch();                         // Update the result list, and match count label.
            return null;                            // And return null, since the overridden method returns an object of the Void class.
        }

        @Override
        protected void done(){  // This method is run when the 'doInBackGround()' method is complete.
            uiReady();          // Set the UI to the ready state.
        }
    }

    /**
     * This class functions as the ActionListener for the radio buttons. This class could be replaced by a
     * method that would get the selected radio button from the button group, and then do a switch/case on the
     * actionCommand of the selected radio button. This would limit the changes made to the searchStyle
     * variable.
     * 
     * @deprecated
     *             Class is now deprecated, as the aforementioned method has been implemented.
     * @author KHR
     */
    @Deprecated
    private class StyleListener implements ActionListener{
        @Override
        public void actionPerformed( ActionEvent e ){
            switch( e.getActionCommand() ){
                case CONTAINS:
                    searchStyle = Style.CONTAIN;
                    break;
                case BEGINS:
                    searchStyle = Style.BEGIN;
                    break;
                case ENDS:
                    searchStyle = Style.END;
                    break;
                case EXACT:
                    searchStyle = Style.EXACT;
                    break;
            }
        }
    }

    @Override
    public void propertyChange( PropertyChangeEvent evt ){  // This class is used to monitor changes in progress made by the SearchTask. (Used to update the progressbar)
        if( searching && evt.getPropertyName().equals( "progress" ) ){ // Checks to see if we're searching and if the changed property of the task is 'property'
            int progress = (Integer) evt.getNewValue(); // Save progress to a variable
            progressBar.setValue( progress );   // Set the value of the progressbar to the progress of the SearchTask
            String progressTxt = String.format( "Search %d%% complete.", search.getProgress() );    // Creates some fancy text
            progressBar.setString( progressTxt );   // Paints some fancy text on the progressbar
        }
    }

    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい
    // # ------------------- BORING GUI STUFF ------------------- #
    // いいいいいいいいいいいいいいいいいいいいいいいいいいいいいい 

    /**
     * Applies the given WindowState to the window.
     * 
     * @param state the WindowState to set.
     */
    private void applyWindowState( WindowState state ){
        try{
            this.state = state;
            wm.loadFile( this.state.getFile() );

            if( this.state.getBounds() != null ) frmWordSearch.setBounds( this.state.getBounds() );
            else frmWordSearch.setLocationRelativeTo( null );
            chckbxmntmShowFilePrompt.setSelected( this.state.doShowStartScreen() );
        }
        catch( FileNotFoundException e ){
            // Hopefully we won't reach this, as we've taken precautions in the layers below.
            ExceptionHandler.handle( e );
        }
    }
    
    /**
     * Initialize the components.
     */
    private void initialize(){
        /* ----- WINDOW ----- */
        // Size, bounds & title
        frmWordSearch = new JFrame();
        frmWordSearch.setMinimumSize( new Dimension( 370, 385 ) );
        frmWordSearch.setPreferredSize( new Dimension( 370, 385 ) );
        frmWordSearch.setTitle( "Word Search" );
        frmWordSearch.setBounds( 100, 100, 370, 385 );
        // Window Listener
        frmWordSearch.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
        frmWordSearch.addWindowListener( new WindowAdapter(){
            @Override
            public void windowClosing( WindowEvent we ){
                exitRoutine();
            }
        } );

        /* ----- SEARCH ----- */
        lblQuery = new JLabel( "Query:" );
        // Search field
        textQuery = new JTextField();
        textQuery.setColumns( 10 );
        // Search button
        btnSearch = new JButton( "Search" );
        btnSearch.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                searchPressed();
            }
        } );
        // Clear button
        btnClear = new JButton( "Clear" );
        btnClear.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                clearSearch();
            }
        } );
        // Search type
        //    - Panel
        pnlSearchType = new JPanel();
        pnlSearchType.setBorder( new TitledBorder( null,
                                                   "Search type",
                                                   TitledBorder.LEADING,
                                                   TitledBorder.TOP,
                                                   null,
                                                   null ) );

        //    - Create radiobuttons
        rdbtnContains = new JRadioButton( "Contains" );
        rdbtnContains.setSelected( true );
        rdbtnContains.setActionCommand( CONTAINS );

        rdbtnBeginsWith = new JRadioButton( "Begins with" );
        rdbtnBeginsWith.setActionCommand( BEGINS );

        rdbtnEndsWith = new JRadioButton( "Ends with" );
        rdbtnEndsWith.setActionCommand( ENDS );

        rdbtnExact = new JRadioButton( "Exact" );
        rdbtnExact.setActionCommand( EXACT );

        //    - Add radiobuttons to buttongroup
        buttonGroup.add( rdbtnContains );
        buttonGroup.add( rdbtnBeginsWith );
        buttonGroup.add( rdbtnEndsWith );
        buttonGroup.add( rdbtnExact );

        //    - Add actionlistener to the radiobuttons
        rdbtnContains.addActionListener( styleListener );
        rdbtnBeginsWith.addActionListener( styleListener );
        rdbtnEndsWith.addActionListener( styleListener );
        rdbtnExact.addActionListener( styleListener );

        // Case sensitivity
        pnlStyle = new JPanel();
        pnlStyle.setBorder( new TitledBorder( null, "Style", TitledBorder.LEADING, TitledBorder.TOP, null, null ) );

        chckbxCaseSensitive = new JCheckBox( "Case sensitive" );

        /* ----- RESULT ----- */
        lblResult = new JLabel( "Result:" );
        // Limitation
        pnlLimitation = new JPanel();
        pnlLimitation.setBorder( new TitledBorder( null,
                                                   "Limitation",
                                                   TitledBorder.LEADING,
                                                   TitledBorder.TOP,
                                                   null,
                                                   null ) );
        comboBox = new JComboBox<Object>();
        comboBox.setModel( new DefaultComboBoxModel( new String[]{ "None", "10", "20", "50", "100" } ) ); // Limitation options
        comboBox.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                limitationBoxActionPerformed();
            }
        } );

        // Matches
        lblMatches = new JLabel( "Matches:" );
        lblMatchcount = new JLabel( "0" );
        lblMatchcount.setHorizontalAlignment( SwingConstants.TRAILING );

        // Result list
        scrollPane = new JScrollPane();
        lstResult = new JList<Object>();
        lstResult.addListSelectionListener( new ListSelectionListener(){
            @Override
            public void valueChanged( ListSelectionEvent lse ){
                resultSelected();
            }
        } );
        scrollPane.setViewportView( lstResult );
        lstResult.setModel( lstModel );
        lstResult.setBorder( new BevelBorder( BevelBorder.LOWERED, null, null, null, null ) );

        // Progressbar
        progressBar = new JProgressBar();
        progressBar.setStringPainted( true );
        uiReady();

        // Menu
        menuBar = new JMenuBar();
        frmWordSearch.setJMenuBar( menuBar );

        //    - File
        mnFile = new JMenu( "File" );
        menuBar.add( mnFile );

        mntmOpenFile = new JMenuItem( "Open File..." );
        mntmOpenFile.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                openFile();
            }
        } );
        mnFile.add( mntmOpenFile );

        separator_1 = new JSeparator();
        mnFile.add( separator_1 );

        mntmExit = new JMenuItem( "Exit" );
        mntmExit.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                exitRoutine();
            }
        } );
        mnFile.add( mntmExit );

        //    - Help
        mnHelp = new JMenu( "Help" );
        menuBar.add( mnHelp );

        chckbxmntmShowFilePrompt = new JCheckBoxMenuItem( "Show file prompt at startup" );
        mnHelp.add( chckbxmntmShowFilePrompt );

        separator = new JSeparator();
        mnHelp.add( separator );

        mntmAbout = new JMenuItem( "About WordSearch" );
        mntmAbout.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                new AboutDialog( frmWordSearch );
            }
        } );
        mnHelp.add( mntmAbout );
    }

    /**
     * This method places the components in their containers and positions them in fancy ways.
     */
    private void placeComponents(){
        GroupLayout groupLayout = new GroupLayout(frmWordSearch.getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblQuery)
                            .addPreferredGap(ComponentPlacement.UNRELATED)
                            .addComponent(textQuery, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblResult)
                            .addPreferredGap(ComponentPlacement.UNRELATED)
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addGroup(groupLayout.createSequentialGroup()
                                    .addComponent(lblMatches)
                                    .addPreferredGap(ComponentPlacement.RELATED)
                                    .addComponent(lblMatchcount, GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE))
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))))
                    .addGap(18)
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                        .addComponent(pnlLimitation, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlStyle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(btnSearch)
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(btnClear))
                        .addComponent(pnlSearchType, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(19))
                .addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(btnClear)
                                .addComponent(btnSearch))
                            .addPreferredGap(ComponentPlacement.UNRELATED)
                            .addComponent(pnlSearchType, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(pnlStyle, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(pnlLimitation, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addComponent(lblQuery)
                                .addComponent(textQuery, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(8)
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
                                .addComponent(lblResult))
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblMatchcount, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblMatches))
                            .addGap(3)))
                    .addGap(13)
                    .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
        
        GroupLayout gl_pnlLimitation = new GroupLayout(pnlLimitation);
        gl_pnlLimitation.setHorizontalGroup(
            gl_pnlLimitation.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_pnlLimitation.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(29, Short.MAX_VALUE))
        );
        gl_pnlLimitation.setVerticalGroup(
            gl_pnlLimitation.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_pnlLimitation.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(36, Short.MAX_VALUE))
        );
        pnlLimitation.setLayout(gl_pnlLimitation);
        
        GroupLayout gl_pnlStyle = new GroupLayout(pnlStyle);
        gl_pnlStyle.setHorizontalGroup(
            gl_pnlStyle.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_pnlStyle.createSequentialGroup()
                    .addComponent(chckbxCaseSensitive)
                    .addContainerGap(19, Short.MAX_VALUE))
        );
        gl_pnlStyle.setVerticalGroup(
            gl_pnlStyle.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_pnlStyle.createSequentialGroup()
                    .addComponent(chckbxCaseSensitive)
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlStyle.setLayout(gl_pnlStyle);
        
        GroupLayout gl_pnlSearchType = new GroupLayout(pnlSearchType);
        gl_pnlSearchType.setHorizontalGroup(
            gl_pnlSearchType.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_pnlSearchType.createSequentialGroup()
                    .addGroup(gl_pnlSearchType.createParallelGroup(Alignment.LEADING)
                        .addComponent(rdbtnContains)
                        .addComponent(rdbtnBeginsWith)
                        .addComponent(rdbtnEndsWith)
                        .addComponent(rdbtnExact))
                    .addContainerGap(7, Short.MAX_VALUE))
        );
        gl_pnlSearchType.setVerticalGroup(
            gl_pnlSearchType.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_pnlSearchType.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(rdbtnContains)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(rdbtnBeginsWith)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(rdbtnEndsWith)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(rdbtnExact)
                    .addContainerGap(35, Short.MAX_VALUE))
        );
        pnlSearchType.setLayout(gl_pnlSearchType);
        frmWordSearch.getContentPane().setLayout(groupLayout);
    }
}