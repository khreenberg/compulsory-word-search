package ui;

import java.awt.Insets;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;

import be.WindowState;
import bll.StateManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * The startup window that asks the user what file to load
 * @author KHR
 *
 */
public class StartupWindow{

    /* Instance fields */
    private static final String WORK_PATH = System.getProperty( "user.dir" );
    private static final String DEFAULT_FILE_NAME = "words.txt";

    /* Instance variables */
    // File path
    private static String filePath = String.format( "%s/%s", WORK_PATH, DEFAULT_FILE_NAME );
    // Window state
    private WindowState state;
    // Components
    private JFrame frmWordSearchSelect;
    private JLabel lblWordFileTo;
    private JTextField txtPath;
    private JButton btnBrowse;
    private JButton btnUseDefault;
    private JCheckBox chckbxDontShowThis;
    private JButton btnOk;

    /**
     * Create the window.
     */
    public StartupWindow() {
        try{
            state = StateManager.loadState();   // Load the WindowState
            if( state.doShowStartScreen() ){    // Check if the startup screen should be shown
                initialize();                   // Initialize the components
                placeComponents();              // Place the components
                /* State Specifics */
                if( state.getFile() == null ){      // If the WindowState doesn't have any file information..
                    btnUseDefault.setText( "Use Default" ); // ..set the text of the default button to 'Use Default'
                }else{                              // Otherwise..
                    btnUseDefault.setText( "Use Last" );    // ..set the text to 'Use Last' and..
                    filePath = state.getFile().getAbsolutePath();   // ..set the variable 'filepath' to the file stored in the WindowState
                }
                /* Show the window */
                frmWordSearchSelect.setVisible( true );
            }else startApp();                   // If the start up screen should not be show, just launch the application.
        }
        catch( ClassNotFoundException | IOException e ){    // Catch some exceptions that won't happen.
            // Getting here is unlikely, as it's prevented in the DAL-layer.
            ExceptionHandler.handle( e );
        }

    }

    /**
     * Method to run when the OK button is pressed.
     */
    private void btnOkPressed(){
        if( !txtPath.getText().isEmpty() ){
            File file = new File( txtPath.getText() );
            if( !file.exists() ){
                String messageString = String.format( "'%s' is a folder, or does not exist!", file.getAbsoluteFile() );
                JOptionPane.showMessageDialog( null, messageString, "No such file!", JOptionPane.WARNING_MESSAGE );
            }else{
                prepareState();
                frmWordSearchSelect.setVisible( false );
                startApp();
            }
        }else{
            // If the user left the path empty, we'll let them know, by using the fancy ExceptionHandler, and a custom FileNotFound exception.
            ExceptionHandler.handle( new FileNotFoundException("You must select a file!") );
        }
    }

    /**
     * Method to run when the Browse button is pressed.
     */
    private void btnBrowsePressed(){

        try{
            JFileChooser jfc = new JFileChooser();
            int option = jfc.showOpenDialog( null );
            if( option == JFileChooser.APPROVE_OPTION ){
                File file = jfc.getSelectedFile();
                state.setFile( file );
                txtPath.setText( file.getPath() );
            }
        }
        catch( Exception e ){
            ExceptionHandler.handle( e );
        }
    }

    /**
     * Method to run when the Default button is pressed.
     */
    private void btnUseDefaultPressed(){
        txtPath.setText( filePath );
    }

    private void startApp(){
        SwingUtilities.invokeLater( new Runnable(){
            @Override
            public void run(){
                try{
                    new WordWindow( state );
                }
                catch( Exception e ){
                    ExceptionHandler.handle( e );
                }
            }
        } );
    }

    /**
     * Prepares the state to be passed on to the main window.
     */
    private void prepareState(){
        state.setShowStartScreen( !chckbxDontShowThis.isSelected() );
        state.setFile( new File( txtPath.getText() ) );
    }

    /**
     * Initialize the contents of the frame.
     * 
     * @wbp.parser.entryPoint
     */
    private void initialize(){
        /* Window stuff */
        frmWordSearchSelect = new JFrame();
        frmWordSearchSelect.setResizable( false );
        frmWordSearchSelect.setTitle( "Word Search: Select word file..." );
        frmWordSearchSelect.setBounds( 100, 100, 315, 180 );
        frmWordSearchSelect.setLocationRelativeTo( null );
        frmWordSearchSelect.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        /* File */
        // Label
        lblWordFileTo = new JLabel( "Word file to search: " );
        // Path text field
        txtPath = new JTextField();
        txtPath.setColumns( 10 );
        // Browse button
        btnBrowse = new JButton( "Browse..." );
        btnBrowse.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                btnBrowsePressed();
            }
        } );
        // Default button
        btnUseDefault = new JButton( "Use Default" );
        btnUseDefault.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                btnUseDefaultPressed();
            }
        } );
        btnUseDefault.setMargin( new Insets( 2, 7, 2, 7 ) ); // Shrinks the button margins slightly to allow for more text.
        btnUseDefault.setMnemonic( 'u' );

        /* Carry on */
        // 'Dont show this at startup'-checkbox
        chckbxDontShowThis = new JCheckBox( "Don't show this at startup" );
        // OK Button
        btnOk = new JButton( "OK" );
        btnOk.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                btnOkPressed();
            }
        } );
    }

    /**
     * Places the components in their containers, and positions them in the window.
     */
    private void placeComponents(){
        GroupLayout groupLayout = new GroupLayout(frmWordSearchSelect.getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(21)
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(chckbxDontShowThis)
                            .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnOk))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblWordFileTo)
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                                .addGroup(groupLayout.createSequentialGroup()
                                    .addComponent(btnBrowse)
                                    .addPreferredGap(ComponentPlacement.RELATED)
                                    .addComponent(btnUseDefault))
                                .addComponent(txtPath))))
                    .addContainerGap(161, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(28)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(txtPath, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblWordFileTo))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnBrowse)
                        .addComponent(btnUseDefault))
                    .addGap(31)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(chckbxDontShowThis)
                        .addComponent(btnOk))
                    .addContainerGap(141, Short.MAX_VALUE))
        );
        frmWordSearchSelect.getContentPane().setLayout(groupLayout);
    }
}
