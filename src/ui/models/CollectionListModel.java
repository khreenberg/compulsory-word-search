package ui.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractListModel;

/**
 * The CollectionListModel is a ListModel that can be based on a Collection.
 * 
 * @author KHR
 * @param <E>
 */
@SuppressWarnings( "serial" )
public class CollectionListModel<E> extends AbstractListModel<E> implements Collection<E>{
    /* The collection the model is based on as a List */
    private List<E> collection;

    /**
     * The constructor for the model.
     * 
     * @param aCollection the collection to base the model on.
     */
    public CollectionListModel( Collection<E> aCollection ) {
        this.collection = new ArrayList<>( aCollection );
        fireContentsChanged( collection, 0, collection.size() - 1 );
    }

    /**
     * Sets a new collection for the model
     * 
     * @param aCollection the collection to set
     */
    public void setCollection( Collection<E> aCollection ){
        this.collection = new ArrayList<>( aCollection );
        fireContentsChanged( collection, 0, collection.size() - 1 );
    }

    @Override
    public E getElementAt( int index ){
        return collection.get( index );
    }

    @Override
    public int getSize(){
        return collection.size();
    }

    @Override
    public boolean add( E object ){
        if( collection.add( object ) ){
            fireContentsChanged( collection, 0, collection.size() - 1 );
            return true;
        }
        return false;
    }

    @Override
    public boolean addAll( Collection<? extends E> aCollection ){
        if( collection.addAll( aCollection ) ){
            fireContentsChanged( collection, 0, collection.size() - 1 );
            return true;
        }
        return false;
    }

    @Override
    public void clear(){
        collection.clear();
        fireContentsChanged( collection, 0, collection.size() - 1 );
    }

    @Override
    public boolean contains( Object object ){
        return collection.contains( object );
    }

    @Override
    public boolean containsAll( Collection<?> aCollection ){
        return collection.containsAll( aCollection );
    }

    @Override
    public boolean isEmpty(){
        return collection.isEmpty();
    }

    @Override
    public Iterator<E> iterator(){
        return collection.iterator();
    }

    @Override
    public boolean remove( Object object ){
        if( collection.remove( object ) ){
            fireContentsChanged( collection, 0, collection.size() - 1 );
            return true;
        }
        return false;
    }

    @Override
    public boolean removeAll( Collection<?> aCollection ){
        if( collection.removeAll( aCollection ) ){
            fireContentsChanged( collection, 0, collection.size() - 1 );
            return true;
        }
        return false;
    }

    @Override
    public boolean retainAll( Collection<?> aCollection ){
        return collection.retainAll( aCollection );
    }

    @Override
    public int size(){
        return collection.size();
    }

    @Override
    public Object[] toArray(){
        return collection.toArray();
    }

    @Override
    public <T> T[] toArray( T[] array ){
        return collection.toArray( array );
    }
}
