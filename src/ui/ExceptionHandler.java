package ui;

import javax.swing.JOptionPane;

/**
 * A single class to handle all the exceptions. This makes it easier to change the error-messages and the way
 * they are displayed. Care should be taken not to include any logic here, like whether to exit the program or
 * something like that.
 * 
 * @author KHR
 */
public class ExceptionHandler{

    /**
     * This is the method that handles the exceptions. It uses the information stored in the exception to
     * generate a descriptive error message popup for the user. If e.getMessage() returns null, a generic
     * message is shown instead, advising the user to restart the application.
     * 
     * @param e the exception to handle.
     */
    public static void handle( Exception e ){
        /* Construct the title for the popup */
        String title = e.getClass().getSimpleName();
        String[] titleWords = title.split( "(?=[A-Z])" ); // Put spaces in the camelCase
        StringBuilder titleBuilder = new StringBuilder(); // Make our titleBuilder
        titleBuilder.append( "Error - " ); // Append the title prefix
        for( String s : titleWords )
            // Append the words from the simple class name, unless the word is "Exception"
            if( !(s.equalsIgnoreCase( "Exception" ) || s.isEmpty()) ) titleBuilder.append( s + " " );
        titleBuilder.deleteCharAt( titleBuilder.length() - 1 ); // Delete the trailing space..
        titleBuilder.append( "!" );                             // ..and substitute it with a '!'

        /* Construct the message body for the popup */
        String msg = e.getLocalizedMessage();                           // Use the message from the exception..
        if( msg == null || msg.isEmpty() ) msg = getDefaultMsg( e );    // ..or generate a generic one, if none is available.

        /* Show the popup */
        JOptionPane.showMessageDialog( null, msg, titleBuilder.toString(), JOptionPane.ERROR_MESSAGE );
    }

    /**
     * This method generates a generic or semi-generic error message based on the given exception.
     * 
     * @param e exception to generate error message for.
     * @return the error message.
     */
    private static String getDefaultMsg( Exception e ){
        /* Initialize the msg-String with a generic error message */
        String msg = "The program has encountered an illegal action!\n"
                     + "You're free to continue, but it might be wise to restart the program.";

        /* List of common exception-names */
        final String fileNotFound = "FileNotFoundException";
        final String ioException = "IOException";
        final String classNotFound = "ClassNotFoundException";

        /* Select the proper message */
        switch( e.getClass().getSimpleName() ){
            case fileNotFound:
                msg = "The specified file was not found.";
                break;
            case ioException:
                msg = "There was an error while accessing the file.";
                break;
            case classNotFound:
                msg = String.format( "The 'system.sys'-file in directory '%s'\n"
                                     + " appears to be corrupted. Please delete it and restart the program.",
                                     System.getProperty( "user.dir" ) );
                break;
        }

        /* Return the message */
        return msg;
    }
}
