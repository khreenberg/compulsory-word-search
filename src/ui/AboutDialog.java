package ui;

import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * The dialog that shows information about the application.
 * @author KHR
 *
 */
@SuppressWarnings( "serial" )
public class AboutDialog extends JDialog{
    
    /* Instance variables */
    private JTextArea textArea;
    private JLabel label;
    private JButton button;

    /**
     * Create the dialog.
     * @param callingWindow the window that made the call to show the about dialog
     */
    public AboutDialog( JFrame callingWindow ) {
        /* Window specifics */
        setTitle( "About" );
        setModal( true );
        setModalityType( ModalityType.APPLICATION_MODAL );
        setBounds(100, 100, 285, 165);
        /* Header */
        label = new JLabel( "About Word Search" );
        label.setFont( new Font( "Tahoma", Font.BOLD, 18 ) );
        /* Text area */
        textArea = new JTextArea();
        textArea.setWrapStyleWord( true );
        textArea.setText( "Word Search was created as a hand in for the first compulsory assignment on second semester SCO." );
        textArea.setLineWrap( true );
        textArea.setFont( new Font( "Tahoma", Font.PLAIN, 13 ) );
        textArea.setEditable( false );
        textArea.setBackground( SystemColor.menu );
        /* Button */
        button = new JButton( "Close" );
        button.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                setVisible( false );
            }
        } );
        /* Layout */
        GroupLayout groupLayout = new GroupLayout(getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGap(113)
                            .addComponent(button, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(textArea, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(label, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(170, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(label, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(textArea, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(button)
                    .addContainerGap(140, Short.MAX_VALUE))
        );
        getContentPane().setLayout(groupLayout);
    	/* Position it, and show it */
    	setLocationRelativeTo( callingWindow );
    	setVisible( true );
    }
}
