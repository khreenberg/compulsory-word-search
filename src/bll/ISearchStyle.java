package bll;

/**
 * This interface gives classes the ability to use the Style enum.
 * 
 * @author KHR
 */
public interface ISearchStyle{

    /**
     * The enum that holds the different search styles.
     */
    enum Style {
        CONTAIN,
        BEGIN,
        END,
        EXACT
    };
}
