package bll;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import be.SearchObject;
import dal.WordAccess;

/**
 * The WordManager class handles all the search queries as well as serving as a bridge between the GUI and
 * Data Access.
 * 
 * @author KHR
 */
public class WordManager{

    /* Instance variables */
    private WordAccess accessor;
    private static WordManager instance = null;

    private List<String> items;
    private List<String> result;

    // Thread specifics
    private int progress = 0;
    private Thread searchThread;

    /**
     * Constructor
     */
    private WordManager() {
        accessor = WordAccess.getInstance();
    }

    /**
     * @return an instance of the WordManager class.
     */
    public static WordManager getInstance(){
        if( instance == null ) instance = new WordManager();
        return instance;
    }

    /**
     * Asks the WordAccess to load a file.
     * 
     * @param path path of the file to load.
     * @throws FileNotFoundException if the file is not found.
     */
    public void loadFile( String path ) throws FileNotFoundException{
        accessor.loadFile( path );
    }

    /**
     * Asks the WordAccess to load a file.
     * 
     * @param f file to load.
     * @throws FileNotFoundException if the file is not found.
     */
    public void loadFile( File f ) throws FileNotFoundException{
        accessor.loadFile( f );
        items = accessor.getItems();
    }

    /**
     * Kills the search task.
     */
    public void killSearch(){
        if( searchThread != null ){
            searchThread.interrupt();
        }
    }

    /**
     * Starts a search based on the specified SearchObject in a new thread.
     * 
     * @param so the SearchObject to base the search on.
     */
    public void doSearch( final SearchObject so ){
        progress = 0;   // Resets the progress to 0
        result = new ArrayList<String>(); // Clear the saved result, if any
        searchThread = new Thread( new Runnable(){  // Make a new thread for the search

            @Override
            public void run(){
                switch( so.getSearchStyle() ){  // Select the proper search method based on the SearchStyle of the SearchObject.
                    case CONTAIN:
                        contain( result, so.getSearchString(), so.isCaseSensitive() );
                        break;
                    case BEGIN:
                        begin( result, so.getSearchString(), so.isCaseSensitive() );
                        break;
                    case END:
                        end( result, so.getSearchString(), so.isCaseSensitive() );
                        break;
                    case EXACT:
                        exact( result, so.getSearchString(), so.isCaseSensitive() );
                        break;
                }
            }
        } );
        searchThread.start();   // Start the search thread
    }

    /**
     * Search for words that contain the search string.
     * @param container the list to modify.
     * @param s the search string.
     * @param caseSensitive whether or not the search should be case sensitive.
     */
    private void contain( List<String> container, String s, boolean caseSensitive ){
        int counter = 0;
        for( String item : items ){
            if( searchThread.isInterrupted() ){
                updateProgress( items.size() );
                return;
            }
            if( !caseSensitive ){
                if( item.toUpperCase().contains( s.toUpperCase() ) ) result.add( item );
            }else{
                if( item.contains( s ) ) result.add( item );
            }
            counter++;
            updateProgress( counter );
        }
    }

    /**
     * Search for words that begin with the specified search string.
     * @param container the list to modify.
     * @param s the search string.
     * @param caseSensitive whether or not the search should be case sensitive.
     */
    private void begin( List<String> container, String s, boolean caseSensitive ){
        int counter = 0;
        for( String item : items ){
            if( !caseSensitive ){
                if( item.toUpperCase().startsWith( s.toUpperCase() ) ) result.add( item );
            }else{
                if( item.startsWith( s ) ) result.add( item );
            }
            counter++;
            updateProgress( counter );
        }
    }

    /**
     * Search for words that end with the specified search string.
     * @param container the list to modify.
     * @param s the search string.
     * @param caseSensitive whether or not the search should be case sensitive.
     */
    private void end( List<String> container, String s, boolean caseSensitive ){
        int counter = 0;
        for( String item : items ){
            if( !caseSensitive ){
                if( item.toUpperCase().endsWith( s.toUpperCase() ) ) result.add( item );
            }else{
                if( item.endsWith( s ) ) result.add( item );
            }
            counter++;
            updateProgress( counter );
        }
    }

    /**
     * Search for words that match the specified search string.
     * @param container the list to modify.
     * @param s the search string.
     * @param caseSensitive whether or not the search should be case sensitive.
     */
    private void exact( List<String> container, String s, boolean caseSensitive ){
        int counter = 0;
        for( String item : items ){
            if( !caseSensitive ){
                if( item.equalsIgnoreCase( s ) ) result.add( item );
            }else{
                if( item.equals( s ) ) result.add( item );
            }
            counter++;
            updateProgress( counter );
        }
    }

    /**
     * @return the search result.
     */
    public List<String> getResult(){
        return result;
    }

    /**
     * @return the last loaded file.
     */
    public File getFile(){
        return accessor.getFile();
    }

    /**
     * @return the progress of the search in percent.
     */
    public int getProgress(){
        return progress;
    }

    /**
     * Calculates and updates the progress of the search.
     * @param searched number of items searched.
     */
    public void updateProgress( int searched ){
        progress = (int) (1.0 * searched / items.size() * 100);
        /* Uncomment the code below to better see the progressbar in work */
        //        try{
        //            if( searched % 50 == 0 ) Thread.sleep( 0, 1 );
        //        }
        //        catch( InterruptedException e ){
        //            searchThread.interrupt(); // Without this line, the search would not be interrupted, as the sleep() would 'take the bullet' for the search thread.
        //        }
    }
}
