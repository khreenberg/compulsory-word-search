package bll;

import java.io.FileNotFoundException;
import java.io.IOException;

import dal.StateAccess;
import be.WindowState;

/**
 * This class facilitates the saving and loading of window states. It serves as the bridge between the GUI and
 * Data Access.
 * 
 * @author KHR
 */
public class StateManager{

    /**
     * Asks the StateAccess class to save the window state.
     * 
     * @param state WindowState to save.
     * @throws FileNotFoundException if the system.sys file isn't found. This is prevented in the StateAccess
     *             class.
     * @throws IOException if there's an error writing to the system.sys file.
     */
    public static void saveState( WindowState state ) throws FileNotFoundException, IOException{
        StateAccess.saveState( state );
    }

    /**
     * Asks the StateAccess class to load the window state.
     * @return the loaded state
     * 
     * @throws FileNotFoundException if the system.sys file isn't found. This is prevented in the StateAccess
     *             class.
     * @throws ClassNotFoundException 
     * @throws IOException if there's an error reading the system.sys file.
     */
    public static WindowState loadState() throws FileNotFoundException, ClassNotFoundException, IOException{
        return StateAccess.loadState();
    }
}
