package be;

import java.awt.Rectangle;
import java.io.File;
import java.io.Serializable;

/**
 * A WindowState object holds all the information needed to save and load the application state. E.g. Window
 * size & position, along with last used file and whether or not to show the startup screen.
 * 
 * @author KHR
 */
@SuppressWarnings( "serial" )
public class WindowState implements Serializable{

    /* Instance variables */
    private Rectangle bounds;
    private File file;
    private boolean showStartScreen;

    /**
     * Constructor
     * @param bounds the bounds of the window.
     * @param file the last opened file.
     * @param showStartScreen whether or not to show the startup screen.
     */
    public WindowState( Rectangle bounds, File file, boolean showStartScreen ) {
        this.bounds = bounds;
        this.file = file;
        this.showStartScreen = showStartScreen;
    }

    /**
     * @return the bounds.
     */
    public Rectangle getBounds(){
        return bounds;
    }

    /**
     * @return the file.
     */
    public File getFile(){
        return file;
    }

    /**
     * @return whether or not to show the startup screen.
     */
    public boolean doShowStartScreen(){
        return showStartScreen;
    }

    /**
     * @param bounds the new bounds.
     */
    public void setBounds( Rectangle bounds ){
        this.bounds = bounds;
    }

    /**
     * @param file the new file.
     */
    public void setFile( File file ){
        this.file = file;
    }

    /**
     * @param showStartScreen whether or not to show the startup screen.
     */
    public void setShowStartScreen( boolean showStartScreen ){
        this.showStartScreen = showStartScreen;
    }
}
