package be;

import bll.ISearchStyle;

/**
 * A SearchObject holds all the information needed to perform a search. E.g. the string to search for, whether
 * the search is case sensitive or not, and which type of search to perform (Contains, Ends With, Begins
 * With...)
 * 
 * @author KHR
 */
public class SearchObject implements ISearchStyle{

    /* Instance variables */
    private String searchString;
    private Style searchStyle;
    private boolean caseSensitive;

    /**
     * Constructor
     * 
     * @param searchString the string to search for.
     * @param searchStyle the type of search to perform.
     * @param caseSensitive whether or not the search is case sensitive.
     */
    public SearchObject( String searchString, Style searchStyle, boolean caseSensitive ) {
        this.searchString = searchString;
        this.searchStyle = searchStyle;
        this.caseSensitive = caseSensitive;
    }

    /**
     * @return the search string.
     */
    public String getSearchString(){
        return searchString;
    }

    /**
     * @return the search style.
     */
    public Style getSearchStyle(){
        return searchStyle;
    }

    /**
     * @return whether or not the search is case sensitive.
     */
    public boolean isCaseSensitive(){
        return caseSensitive;
    }
}
